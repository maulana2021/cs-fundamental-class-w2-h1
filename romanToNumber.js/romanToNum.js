function romanToNum (roman) {
    const romanMapping = ["I","V","X","L","C","D","M"]
    const valueMapping = [1,5,10,50,100,500,1000]

    let result = 0;

    for (i=0; i<roman.length; i++) {
        const currentRomanIndex = romanMapping.indexOf(roman[i]);
        const nextRomanIndex = romanMapping.indexOf(roman[i+1]);

        const currentValueIndex = valueMapping[currentRomanIndex];
        const nextValueIndex = valueMapping[nextRomanIndex];


        if(currentValueIndex < nextValueIndex){
            result -= currentValueIndex;
        } else {
            result += currentValueIndex;
        }
    }
    return result;
}

