function toBinary(n){
    var s = n.toString(2);
    var n = Number(s);
    return n;
}

console.log (toBinary(1), 1);
console.log (toBinary(2), 10);
console.log (toBinary(3), 11);
console.log (toBinary(5), 101);