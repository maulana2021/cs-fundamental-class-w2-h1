const list = document.querySelector(".list");

// eslint-disable-next-line no-unused-vars
const sort_name_btn = document.querySelector(".sort-options .sort-name");
// eslint-disable-next-line no-unused-vars
const sort_meta_btn = document.querySelector(".sort-options .sort-meta");
// eslint-disable-next-line no-unused-vars
const sort_age_btn = document.querySelector(".sort-options .sort-age");

let list_item = [
  {
    name: "Batman",
    meta: "Black",
    age: "57",
  },
  {
    name: "Superman",
    meta: "Red, Blue",
    age: "500",
  },
  {
    name: "Wonder Woman",
    meta: "Gold, Red & Blue",
    age: "450",
  },
  {
    name: "The Flash",
    meta: "Red, Yellow",
    age: "24",
  },
  {
    name: "Hulk",
    meta: "Green",
    age: "45",
  },
  {
    name: "Green Lanter",
    meta: "Green & Black",
    age: "28",
  },
  {
    name: "Nightwing",
    meta: "Blue & Black",
    age: "30",
  },
];

function displayList(arr) {
  list.innerHTML = "";

  for (let i = 0; i < arr.length; i++) {
    let item = arr[i];

    let item_element = document.createElement("div");
    item_element.classList.add("list_item");

    let title = document.createElement("div");
    title.classList.add("item-title");
    title.innerText = item.name;

    item_element.appendChild(title);

    let meta = document.createElement("div");
    meta.classList.add("item-meta");
    meta.innerText = item.meta;

    item_element.appendChild(meta);

    let age = document.createElement("div");
    meta.classList.add("item-age");
    meta.innerText = item.age;

    item_element.appendChild(age);

    list.appendChild(item_element);
  }
}

displayList(list_item);

console.log(list_item);
// ! penting
// ? apa ini
// todo kerjakan
// * info penting
