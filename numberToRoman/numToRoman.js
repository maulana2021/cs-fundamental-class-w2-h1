function numberToRoman (number) {
    const romanMapping = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];
    const valueMapping = [1,4,5,9,10,40,50,90,100,400,500,900,1000];
    const length = valueMapping.length-1;
    let result = "";
    for (i = length; i>=0; i--) {
        
        const Roman = romanMapping[i];
        const value = valueMapping[i];

        console.log (value);
        const x = Math.floor(number / value);
        for (j=0;j<x; j++) {
            result = result + Roman;
        }

        number = number - x * value;
    }
    return result;
}
console.log (numberToRoman (4243));

